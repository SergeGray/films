class MoviesController < ApplicationController
  before_action :set_movie, only: [:show, :edit, :update, :destroy, :assign_job, :create_job, :edit_job, :update_job, :destroy_job, :add_video, :upload_video, :show_video, :delete_video, :update_video]

  # GET /movies
  # GET /movies.json
  def index
    @movies = Movie.all
    @people = Person.all
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
    @people = Person.all
    @videos = @movie.videos.all
  end

  # GET /movies/new
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
  end

  def assign_job
    @people = Person.all
  end

  def edit_job
    @job = Job.find(params[:job_id])
    @person = Person.find(@job.person_id)
  end

  def add_video
  end

  def show_video
    @video = Video.find(params[:video_id])
  end

  def update_video
    @video = Video.find(params[:video_id])
    @video.properties = params[:video][:properties].to_unsafe_h

    respond_to do |format|
      if @video.save     
        format.html { redirect_to @movie, notice: 'Video properties were successfully updated.' }
        format.json { render :show_video, status: :ok, location: @movie }
      else
        format.html { render :show_video }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def upload_video
    @movie.videos.create(movie_params[:videos])
    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Video was successfully uploaded.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :add_video }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_job
    @job = Job.find(params[:job_id])
    @movie.update_position(@job.id, movie_params[:job][:position])
    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Job was successfully edited.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :edit_job }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_job
    @movie.add_position(movie_params[:person][:person_id], movie_params[:person][:jobs][:position])
    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Job was successfully assigned.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :assign_job }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end



  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movies/1
  # PATCH/PUT /movies/1.json
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
        format.json { render :show, status: :ok, location: @movie }
      else
        format.html { render :edit }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.json
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_url, notice: 'Movie was successfully destroyed.' }
      format.json { render :show, status: :ok, location: @movie }
    end
  end

  def destroy_job
    Job.find(params[:job_id]).destroy
    respond_to do |format|
      format.html { redirect_to @movie, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_video
    Video.find(params[:video_id]).destroy
    respond_to do |format|
      format.html { redirect_to @movie, notice: 'Video was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:title, :job_id, :video_id, person: [:person_id, jobs: [:position]], job: [:position, :id], videos: [:video_file]) 
    end
end
