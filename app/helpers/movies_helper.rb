module MoviesHelper
  def assign_job_path(id)
    "/movies/#{id}/assign_job"
  end

  def edit_job_path(id, job_id)
    "/movies/#{id}/edit_job/#{job_id}"
  end

  def destroy_job_path(id, job_id)
    "/movies/#{id}/destroy_job/#{job_id}"
  end

  def add_video_path(id)
    "/movies/#{id}/add_video"
  end

  def show_video_path(id, video_id)
    "/movies/#{id}/show_video/#{video_id}"
  end
  
  def delete_video_path(id, video_id)
    "/movies/#{id}/delete_video/#{video_id}"
  end

  def update_video_path(id, video_id)
    "/movies/#{id}/update_video/#{video_id}"
  end
end
