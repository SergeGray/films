class Job < ApplicationRecord
  belongs_to :movie
  belongs_to :person
  validates :position, presence: true
end
