class Video < ApplicationRecord
  has_attached_file :video_file
  do_not_validate_attachment_file_type :video_file
  belongs_to :movie
  serialize :properties, Hash
end
