class Person < ApplicationRecord
  has_many :movies, through: :jobs
  has_many :jobs, dependent: :destroy
  validates :name, presence: true
  accepts_nested_attributes_for :movies, :jobs
end
