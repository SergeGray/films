class Movie < ApplicationRecord
  has_many :people, through: :jobs
  has_many :jobs, dependent: :destroy
  has_many :videos
  validates :title, presence: true
  validates_associated :jobs
  accepts_nested_attributes_for :jobs, :people

  def update_position(job_id, position)
    job = Job.find job_id

    job.position = position
    job.save
  end

  def add_position(person_id, position)
    jobs.create person_id: person_id, position: position
  end
end
