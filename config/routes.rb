Rails.application.routes.draw do

  resources :movies do
    member do
      get 'assign_job'
      post 'create_job'
      get 'add_video'
      post 'upload_video'
      get 'edit_job/:job_id', action: 'edit_job'
      post 'update_job/:job_id', action: 'update_job'
      delete 'destroy_job/:job_id', action: 'destroy_job'
      get 'show_video/:video_id', action: 'show_video'
      delete 'delete_video/:video_id', action: 'delete_video'
      post 'update_video/:video_id', action: 'update_video'
    end
  end
  resources :people

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'movies#index'
end
