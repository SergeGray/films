class AddMovieReferencesToVideos < ActiveRecord::Migration[5.0]
  def change
    add_reference :videos, :movie, foreign_key: true
  end
end
