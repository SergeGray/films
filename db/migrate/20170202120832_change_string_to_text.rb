class ChangeStringToText < ActiveRecord::Migration[5.0]
  def change
    remove_column :videos, :parameters, :string
    add_column :videos, :properties, :text
  end
end
