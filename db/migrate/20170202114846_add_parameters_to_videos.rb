class AddParametersToVideos < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :parameters, :string
  end
end
