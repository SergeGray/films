class AddAttachmentToVideo < ActiveRecord::Migration[5.0]
  def change
    add_attachment :videos, :video_file
  end
end
