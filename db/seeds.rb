# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

people = Person.create([
  {name: 'Christian Bale'}, 
  {name: 'Anne Hathaway'}, 
  {name: 'Michael Caine'}, 
  {name: 'Tom Hardy'}, 
  {name: 'Joseph Gordon-Levitt'}, 
  {name: 'Morion Cotillard'}, 
  {name: 'Cillian Murphy'}, 
  {name: 'Christopher Nolan'}
])
movies = Movie.create([
  {title: 'The Prestige'}, 
  {title: 'The Dark Knight Rises'}, 
  {title: 'Interstellar'}, 
  {title: 'Inception'}
])
jobs = Job.create([
  {position: 'Cast', movie: movies.first, person: people.first}, 
  {position: 'Cast', movie: movies.second, person: people.first}, 
  {position: 'Cast', movie: movies.second, person: people.second}, 
  {position: 'Cast', movie: movies.third, person: people.second}, 
  {position: 'Cast', movie: movies.first, person: people.third}, 
  {position: 'Cast', movie: movies.second, person: people.third}, 
  {position: 'Cast', movie: movies.third, person: people.third}, 
  {position: 'Cast', movie: movies.fourth, person: people.third}, 
  {position: 'Cast', movie: movies.second, person: people.fourth}, 
  {position: 'Cast', movie: movies.fourth, person: people.fourth}, 
  {position: 'Cast', movie: movies.second, person: people.fifth},
  {position: 'Cast', movie: movies.fourth, person: people.fifth},
  {position: 'Cast', movie: movies.second, person: people[5]},
  {position: 'Cast', movie: movies.fourth, person: people[5]},
  {position: 'Cast', movie: movies.second, person: people[6]},
  {position: 'Cast', movie: movies.fourth, person: people[6]},
  {position: 'Director', movie: movies.first, person: people.last},
  {position: 'Director', movie: movies.second, person: people.last},
  {position: 'Director', movie: movies.third, person: people.last},
  {position: 'Director', movie: movies.fourth, person: people.last}
])
