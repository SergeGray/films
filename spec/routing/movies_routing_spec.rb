require "rails_helper"

RSpec.describe MoviesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movies").to route_to("movies#index")
    end

    it "routes to #new" do
      expect(:get => "/movies/new").to route_to("movies#new")
    end

    it "routes to #show" do
      expect(:get => "/movies/1").to route_to("movies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movies/1/edit").to route_to("movies#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movies").to route_to("movies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/movies/1").to route_to("movies#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/movies/1").to route_to("movies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movies/1").to route_to("movies#destroy", :id => "1")
    end
    
    it "routes to #destroy_job" do
      expect(:delete => "/movies/1/destroy_job/2").to route_to("movies#destroy_job", :id => "1", :job_id => "2")
    end

    it "routes to #update_job" do
      expect(:post => "/movies/1/update_job/2").to route_to("movies#update_job", :id => "1", :job_id => "2")
    end

    it "routes to #create_job" do
      expect(:post => "/movies/1/create_job").to route_to("movies#create_job", :id => "1")
    end

    it "routes to #edit_job" do
      expect(:get => "/movies/1/edit_job/2").to route_to("movies#edit_job", :id => "1", :job_id => "2")
    end

    it "routes to #assign_job" do
      expect(:get => "/movies/1/assign_job").to route_to("movies#assign_job", :id => "1")
    end

    it "routes to #add_video" do
      expect(:get => "/movies/1/add_video").to route_to("movies#add_video", :id => "1")
    end
    
    it "routes to #upload_video" do
      expect(:post => "/movies/1/upload_video").to route_to("movies#upload_video", :id => "1")
    end

    it "routes to #show_video" do
      expect(:get => "/movies/1/show_video/2").to route_to("movies#show_video", :id => "1", :video_id => "2")
    end

    it "routes to #delete_video" do
      expect(:delete => "/movies/1/delete_video/2").to route_to("movies#delete_video", :id => "1", :video_id => "2")
    end

    it "routes to #update_video" do
      expect(:post => "/movies/1/update_video/2").to route_to("movies#update_video", :id => "1", :video_id => "2")
    end
  end
end
