require 'rails_helper'

RSpec.describe Video, type: :model do
  it "has a video_file" do
    expect(Video).to have_attached_file(:video_file)
  end

  it "serializes a hash" do
    movie = Movie.create title: "Titanic"
    video = Video.create movie_id: movie.id, properties: Hash[ resolution: "1280x720", container: "mp4" ]
    expect(video.properties).to be_a Hash
  end
end
