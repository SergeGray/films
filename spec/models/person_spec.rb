require 'rails_helper'

RSpec.describe Person, type: :model do
  let(:bob) { Person.create!(:name => "Bob Bobson") }

  it "can be created with :name" do
    expect(bob).to be_kind_of(Person)
  end
end

