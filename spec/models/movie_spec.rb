require 'rails_helper'

RSpec.describe Movie, type: :model do
  let(:titanic) { Movie.create!(:title => "Titanic") }

  it "can be created with :title" do
    expect(titanic).to be_kind_of(Movie)
  end

  describe "update_position" do
    let (:bob) { Person.create name: "Bob" }

    context "when a movie has a person assigned" do
      let! (:bobs_position) { titanic.jobs.create person_id: bob.id, position: "Director" }

      it "can change a job name" do
        titanic.update_position bobs_position.id, "Cast"

        expect(bobs_position.reload.position).to eq "Cast"
      end
    end
  end

  describe "add_position" do
    let (:bob) { Person.create name: "Bob" }

    context "when a movie has no people assigned" do
      it "can assign a person to a movie" do
        titanic.add_position(bob.id, "Director")

        expect(titanic.jobs.count).to eq 1
      end
    end
  end

end
