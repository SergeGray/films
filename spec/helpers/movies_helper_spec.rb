require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the MoviesHelper. For example:
#
# describe MoviesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe MoviesHelper, type: :helper do
  describe "assign_job_path" do
    it "returns assign_job path" do
      expect(helper.assign_job_path(1)).to eq "/movies/1/assign_job"
    end
  end
  describe "edit_job_path" do
    it "returns edit_job path" do
      expect(helper.edit_job_path(1, 2)).to eq "/movies/1/edit_job/2"
    end
  end
  describe "destroy_job_path" do
    it "returns destroy_job path" do
      expect(helper.destroy_job_path(1, 2)).to eq "/movies/1/destroy_job/2"
    end
  end
  describe "add_video_path" do
    it "returns add_video path" do
      expect(helper.add_video_path(1)).to eq "/movies/1/add_video"
    end
  end
  describe "show_video_path" do
    it "returns show_video path" do
      expect(helper.show_video_path(1, 2)).to eq "/movies/1/show_video/2"
    end
  end
  describe "update_video_path" do
    it "returns update_video path" do
      expect(helper.update_video_path(1, 2)).to eq "/movies/1/update_video/2"
    end
  end
  describe "delete_video_path" do
    it "returns delete_video path" do
      expect(helper.delete_video_path(1, 2)).to eq "/movies/1/delete_video/2"
    end
  end

end
