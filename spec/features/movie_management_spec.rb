require 'rails_helper'

RSpec.feature "movie management" do

  it "has movie as an index page" do
    Movie.create(title: 'Frozen')
    visit '/'
        
    expect(page).to have_text('Movies Frozen')
  end
  
  it "redirects to movies/new" do
    visit '/'

    click_link("New Movie")

    expect(current_path).to eq('/movies/new')
  end

  it "can create new movies and adds them to the database" do
    visit '/movies/new'

    fill_in 'movie_title', with: 'Dumbo'

    click_button 'Create Movie'

    expect(current_path).to eq('/movies/1')
    expect(page).to have_text('Dumbo')
    expect(Movie.find(1).title).to eq('Dumbo')
  end

  it "prevents you from creating invalid movies" do
    visit '/movies/new'

    fill_in 'movie_title', with: nil

    click_button 'Create Movie'

    expect(page).to have_text('error')
    expect{Movie.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end


  it "lets you view movie" do
    Movie.create(title: 'Frozen')
    visit '/'

    click_link("Show")

    expect(current_path).to eq('/movies/1')
  end

  it "shows assigned jobs" do
    movie = Movie.create(title: 'Titanic')
    person = Person.create(name: 'Leonardo')
    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')

    visit '/movies/1'

    expect(page).to have_text('Leonardo Cast')
  end


  it "redirects to movies/edit" do
    Movie.create(title: 'Frozen')
    visit '/'

    click_link("Edit")

    expect(current_path).to eq('/movies/1/edit')
  end
 
  it "lets you edit movies" do
    Movie.create(title: 'Frozen')
    visit '/movies/1/edit'

    fill_in 'movie_title', with: 'Disney Movie'

    click_button 'Update Movie'
    
    expect(current_path).to eq('/movies/1')
    expect(page).to have_text('Disney Movie')
    expect(Movie.find(1).title).to eq('Disney Movie')
  end

  it "prevents you from doing invalid movie edits" do
    Movie.create(title: 'Frozen')
    visit '/movies/1/edit'

    fill_in 'movie_title', with: nil

    click_button 'Update Movie'
    
    expect(page).to have_text('error')
    expect(Movie.find(1).title).to eq('Frozen')
  end

  it "can delete a movie" do
    Movie.create(title: 'Frozen')
    visit '/'

    click_link 'Destroy'

    expect(page).to have_text('destroyed')
    expect{Movie.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end


end

