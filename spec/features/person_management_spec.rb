require 'rails_helper'

RSpec.feature "person management" do
 
  it "redirects to person/new" do
    visit '/people'

    click_link("New Person")

    expect(current_path).to eq('/people/new')
  end

  it "can create new people and adds them to the database" do
    visit '/people/new'

    fill_in 'person_name', with: 'Joe'

    click_button 'Create Person'

    expect(current_path).to eq('/people/1')
    expect(page).to have_text('Joe')
    expect(Person.find(1).name).to eq('Joe')
  end

  it "prevents you from creating invalid people" do
    visit '/people/new'

    fill_in 'person_name', with: nil

    click_button 'Create Person'

    expect(page).to have_text('error')
    expect{Person.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end


  it "lets you view person" do
    Person.create(name: 'Jack')
    visit '/people'

    click_link("Show")

    expect(current_path).to eq('/people/1')
  end

  it "shows assigned jobs" do
    movie = Movie.create(title: 'Titanic')
    person = Person.create(name: 'Leonardo')
    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')

    visit '/people/1'

    expect(page).to have_text('Titanic Cast')
  end

  it "redirects to people/:id/edit" do
    Person.create(name: 'Jack')
    visit '/people'

    click_link("Edit")

    expect(current_path).to eq('/people/1/edit')
  end
 
  it "lets you edit people" do
    Person.create(name: 'Jack')
    visit '/people/1/edit'

    fill_in 'person_name', with: 'Jackie'

    click_button 'Update Person'
    
    expect(current_path).to eq('/people/1')
    expect(page).to have_text('Jackie')
    expect(Person.find(1).name).to eq('Jackie')
  end

  it "prevents you from doing invalid person edits" do
    Person.create(name: 'Jack')
    visit '/people/1/edit'

    fill_in 'person_name', with: nil

    click_button 'Update Person'
    
    expect(page).to have_text('error')
    expect(Person.find(1).name).to eq('Jack')
  end

  it "can delete a person" do
    Person.create(name: 'Jack')
    visit '/people'

    click_link 'Destroy'

    expect(page).to have_text('destroyed')
    expect{Person.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end


end

