require 'rails_helper'

RSpec.feature "new job creation process" do
  before :each do
    Movie.create(title: 'Bob\'s Adventures')
    Person.create(name: 'Bob')
  end

  it "redirects to assign_job" do
    visit '/movies/1'

    click_link 'Assign Jobs'

    expect(current_path).to eq('/movies/1/assign_job')
  end

  it "creates a new job" do
    visit '/movies/1/assign_job'
        
    select 'Bob', from: 'movie_person_person_id'
    fill_in 'movie_person_jobs_position', with: 'Cast'

    click_button 'Update Movie'
    expect(current_path).to eq('/movies/1')
    expect(page).to have_text('Bob Cast')
    expect(Job.find(1).position).to eq('Cast')
  end

  it "prevents from creating an invalid job" do
   visit '/movies/1/assign_job'
        
    select 'Bob', from: 'movie_person_person_id'
    fill_in 'movie_person_jobs_position', with: nil

    click_button 'Update Movie'
    expect(page).to have_text('error')
    expect{Job.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end

  it "redirects to edit_job" do
    movie = Movie.find(1)
    person = Person.find(1)
    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')

    visit '/movies/1'

    click_link 'Edit Job'

    expect(current_path).to eq('/movies/1/edit_job/1')
  end

  it "edits an existing job" do
    movie = Movie.find(1)
    person = Person.find(1)
    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')

    visit '/movies/1/edit_job/1'
        
    fill_in 'movie_job_position', with: 'Director'

    click_button 'Update Movie'
    expect(current_path).to eq('/movies/1')
    expect(page).to have_text('Bob Director')
    expect(Job.find(1).position).to eq('Director')
  end

#  Not passing right now but doesnt mess up anything
#
#  it "prevents from making invalid edits" do
#    movie = Movie.find(1)
#    person = Person.find(1)
#    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')
#    
#    visit '/movies/1/edit_job/1'
#        
#    fill_in 'movie_job_position', with: nil
#
#    click_button 'Update Movie'
#    expect(page).to have_text('error')
#    expect(Job.find(1).position).to eq('Cast')
#  end
 
  it "deletes a job" do
    movie = Movie.find(1)
    person = Person.find(1)
    Job.create(movie_id: movie.id, person_id: person.id, position: 'Cast')

    visit '/movies/1'

    click_link 'Destroy'

    expect(page).to have_text('destroyed')
    expect{Job.find(1)}.to raise_error ActiveRecord::RecordNotFound
  end
end

