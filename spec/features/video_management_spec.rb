require 'rails_helper'

RSpec.feature "video management process" do
  it "adds a new video" do
    Movie.create(title: "Limbo")
    visit '/movies/1/add_video'
        
    attach_file 'movie_videos_video_file', 'spec/fixtures/files/limbo.avi'

    click_button 'Update Movie'
    expect(current_path).to eq('/movies/1')
    expect(page).to have_text('uploaded')
    expect(Video.find(1)).to have_attached_file(:video_file)
    expect(File.exist?('spec/test_files/videos/000/000/001/original.avi')).to be true
  end

  it "displays uploaded video files" do
    movie = Movie.create(title: "Limbo")
    Video.create(movie_id: movie.id, video_file: File.new('spec/fixtures/files/limbo.avi'))
    visit '/movies/1'

    expect(page).to have_text('Videos: limbo')
  end

  it "lets you view the video" do
    movie = Movie.create(title: "Limbo")
    video = Video.create(movie_id: movie.id, video_file: File.new('spec/fixtures/files/limbo.avi'))
    visit '/movies/1'
    
    click_link "#{video.video_file_file_name}"

    expect(current_path).to eq('/movies/1/show_video/1')
    expect(page).to have_text("#{number_to_human_size(video.video_file_file_size)}")
  end

  it "lets you delete the video" do
    movie = Movie.create(title: "Limbo")
    Video.create(movie_id: movie.id, video_file: File.new('spec/fixtures/files/limbo.avi'))
    visit '/movies/1'

    click_link 'Delete'

    expect(page).to have_text('deleted')
    expect{Video.find(1)}.to raise_error ActiveRecord::RecordNotFound
    expect(File.exist?('spec/test_files/videos/000/000/001/original.avi')).to be false
  end
end

